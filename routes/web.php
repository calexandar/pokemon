<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home')->name('home');
Route::get('/initialize', 'PagesController@initialize');
Route::get('/flush', 'PagesController@flush');

Route::get('/pokemon/create', 'PokemonController@create')->name('pages.create');
Route::post('/pokemon', 'PokemonController@store')->name('pages.store');
Route::get('/pokemon/not-found', 'PokemonController@notFound')->name('pages.notFound');

Route::get('/pokemon/{title}', 'PokemonController@show')->name('pages.show');


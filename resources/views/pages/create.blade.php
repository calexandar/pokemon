<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Add new Pokemon</title>
</head>

<body>

    <div class="container">
        <div class="row h-100 mt-4">
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Add new Pokemon</h4>

                @if($errors->any())
                    {!!implode('', $errors->all('<div>:message</div>'))!!}
                @endif
                <form action="{{ route('pages.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="image">Image</label>
                            <input type="text" class="form-control" id="image" name="image" value="{{old('image')}}">
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="desc">Description</label>
                            <textarea type="text" class="form-control" id="desc" name="desc" value="{{old('desc')}}"></textarea>
                        </div>
                    </div>

                    <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>

</body>

</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pokemons</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links > a:hover {
                text-decoration: underline;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                @if(session()->has('pokemons'))
                    <div class="title m-b-md">
                        Pokemons
                    </div>

                    <div class="links">
                        @foreach($pokemons as $key => $pokemon)
                            <a href="{{ route('pages.show', [ 'title' => $key ]) }}">{{ $pokemon['title'] }}
                        @endforeach
                    </div>
                    <br><br>
                    <div>
                        <a href="{{ route('pages.create')}}">Add a pokemon</a>
                    </div>
                @else
                    <div>
                        <h3>We don't have any pokemons yet. <a href="{{ route('pages.create')}}" >Would you like to add one</a> </h3>
                    </div>
                @endif
            </div>
        </div>
    </body>
</html>

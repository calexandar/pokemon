<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{

    private $pokemons = [
        'charmeleon' => [
            'title' => 'Charmeleon #005',
            'image' => 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/005.png',
            'desc' => 'Charmeleon mercilessly destroys its foes using its sharp claws. If it
            encounters a strong foe, it turns aggressive. In this excited state, the flame at the
            tip of its tail flares with a bluish white color.'
        ],
        'wartortle' => [
            'title' => 'Wartortle #008',
            'image' => 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/008.png',
            'desc' => 'Its tail is large and covered with a rich, thick fur. The tail
            becomes increasingly deeper in color as Wartortle ages. The scratches on its shell are
            evidence of this Pokémons toughness as a battler.'
        ],
        'butterfree' => [
            'title' => 'Butterfree #012',
            'image' => 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/012.png',
            'desc' => 'Its wings are covered in toxic scales. If it finds bird Pokémon going
            after Caterpie, Butterfree sprinkles its scales on them to drive them off.'
        ],
    ];

    public function home()
    {

        $pokemons = session()->get('pokemons');

        return view('welcome')->with('pokemons', $pokemons);
    }

    public function initialize()
    {
        session()->put(['pokemons' => $this->pokemons]);
        return redirect()->route('home');
    }

    public function flush()
    {
        session()->flush();
        return redirect()->route('home');
    }
}

<?php

namespace App\Http\Controllers;
use App\Http\Requests\PokemonCreateRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PokemonController extends Controller
{
    private $pokemons = [
        'charmeleon' => [
            'title' => 'Charmeleon #005',
            'image' => 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/005.png',
            'desc' => 'Charmeleon mercilessly destroys its foes using its sharp claws. If it encounters a strong foe, it turns aggressive. In this excited state, the flame at the tip of its tail flares with a bluish white color.'
        ],
        'wartortle' => [
            'title' => 'Wartortle #008',
            'image' => 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/008.png',
            'desc' => 'Its tail is large and covered with a rich, thick fur. The tail becomes increasingly deeper in color as Wartortle ages. The scratches on its shell are evidence of this Pokémons toughness as a battler.'
        ],
        'butterfree' => [
            'title' => 'Butterfree #012',
            'image' => 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/012.png',
            'desc' => 'Its wings are covered in toxic scales. If it finds bird Pokémon going after Caterpie, Butterfree sprinkles its scales on them to drive them off.'
        ],
    ];

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Nacin 1:
        $request->validate([
            'title' => 'required|min:5|max:30',
            'image' => 'required|url',
            'desc' => 'required'
        ]);

        // Nacin 2: So koristenje na PokemonCreateRequest

        $pokemon = [
            'title' => $request->input('title'),
            'image' => $request->input('image'),
            'desc' => $request->input('desc')
        ];
        $pokemons = session()->get('pokemons');

        $slug = Str::of($request->input('title'))->slug('-');

        $pokemons[(string)$slug] = $pokemon;
        session()->put(['pokemons' => $pokemons]);

        return redirect()->route('pages.show', ['title' => $slug]);
//        return view('pokemon.show')
//            ->with('title', $pokemon['title'] . ' - pokemon details')
//            ->with('pokemon', $pokemon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        // Is the title defined as key in our array?
//        $pokemon = $this->pokemons[$title]?? null;
        $pokemon = session()->get('pokemons')[$title] ?? null;

        if (!$pokemon) {
            return redirect()->route('pages.notFound');
        }

        return view('pages.show')
            ->with('title', $pokemon['title'] . ' - pokemon details')
            ->with('pokemon', $pokemon);
    }

    public function notFound()
    {
        // For 404 error, we need custom view to handle it
        // https://laravel.com/docs/7.x/errors
        abort(403, 'The pokemon is not found');
    }
}
